#!/usr/bin/make -f

# Uncomment this to turn on verbose mode. 
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

TMP:=$(CURDIR)/debian/tmp

ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

configure: configure-stamp
configure-stamp:
	dh_testdir
	echo "$(DEB_VERSION_UPSTREAM_REVISION)" > VERSION.txt
	echo "boxbackup" >> VERSION.txt
	sh -x ./bootstrap
	./configure $(DEB_EXTRA_CONFIG_FLAGS) LDFLAGS="-Wl,--as-needed"
	touch configure-stamp

build-stamp: configure-stamp
	dh_testdir
	$(MAKE) V=1
# the testsuite is only really maintained on i386 and amd64
ifneq (,$(filter $(DEB_HOST_ARCH),i386 amd64))
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
	./runtest.pl ALL
endif
endif
	touch build-stamp

docs/docbook/instguide.pdf:
	$(MAKE) -C docs instguide
	cd docs/docbook && docbook2pdf instguide.xml

docs/docbook/adminguide.pdf: configure-stamp
	$(MAKE) -C docs adminguide
	cd docs/docbook && docbook2pdf adminguide.xml

docs: docs/docbook/instguide.pdf docs/docbook/adminguide.pdf
	$(MAKE) -C docs manpages

build-arch: build-stamp
build-indep: docs

build: build-arch build-indep

clean:
	dh_testdir
	dh_testroot
	dh_clean build-stamp configure-stamp
	echo "USE_SVN_VERSION" > VERSION.txt
	echo "boxbackup" >> VERSION.txt
	[ ! -f Makefile ] || make clean
	sh debian/clean.sh
	dh_clean config.log config.status 

install: DH_OPTIONS=
install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	mkdir -p $(TMP)/etc/logcheck/ignore.d.workstation
	mkdir -p $(TMP)/etc/logcheck/ignore.d.server
	install -m 644 debian/boxbackup-server.logcheck.ignore $(TMP)/etc/logcheck/ignore.d.workstation/boxbackup-server
	install -m 644 debian/boxbackup-server.logcheck.ignore $(TMP)/etc/logcheck/ignore.d.server/boxbackup-server

	dh_install

binary-indep:
#  no architecture independant packages are being built

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir -a
	dh_testroot -a
	dh_installdebconf -a
	dh_installdocs -a -A ExceptionCodes.txt docs/docbook/instguide.pdf docs/docbook/adminguide.pdf
	dh_installinit -a
	dh_installcron -a
	dh_installman
	dh_installchangelogs  -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-arch
.PHONY: build build-arch build-indep clean binary-indep binary-arch binary install docs
