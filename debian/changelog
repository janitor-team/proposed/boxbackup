boxbackup (0.13~~git20200326.g8e8b63c-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 0.13~~git20200326.g8e8b63c
    - disables leak detection

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 10 May 2020 18:55:55 -0400

boxbackup (0.13~~git20190527.g039c4a1-4) unstable; urgency=medium

  * QA upload.
  * Really upload to unstable.
  * Verified that builds with tests enabled (Closes: #943517)
  * Bump Standards-version (no changes needed)

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 15 Feb 2020 14:07:15 -0500

boxbackup (0.13~~git20190527.g039c4a1-2) experimental; urgency=medium

  * QA upload.
  * Upload to unstable.

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 20 Jul 2019 00:50:07 -0400

boxbackup (0.13~~git20190527.g039c4a1-1) experimental; urgency=medium

  * QA upload.
  * Update to new upstream version 0.13~~git20190527.g039c4a1 Requested by
    upstream, now uses 2048bit keys by default. Keys generated with
    earlier versions are considered "weak" and generate a warning for now.
    (Closes: #907135)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 27 May 2019 18:40:14 -0400

boxbackup (0.13~~git20180819.g2f5b556-1) unstable; urgency=medium

  * New upstream pre-release
  * debian/watch: Track commits on master from github.com

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 10 Mar 2019 14:11:33 -0400

boxbackup (0.13~~git20180313.g16a11e86-2) unstable; urgency=medium

  * QA upload.
  * debian/control: Point Vcs headers to salsa
  * Bugfix compile against openssl1.1 (Closes: #907135).

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 07 Jun 2019 05:55:39 -0400

boxbackup (0.13~~git20180313.g16a11e86-1) unstable; urgency=medium

  * New upstream pre-release (requested by upstream)
  * Fixes test failures on i386 (closes: #890997)

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 14 Mar 2018 08:19:09 -0400

boxbackup (0.13~~git20180303.g6178fd34-1) unstable; urgency=medium

  * New upstream pre-release (requested by upstream)
  * Compile against libssl 1.1 (again) (Closes: #870775)

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 03 Mar 2018 14:08:09 -0500

boxbackup (0.13~~git20180102.g6d7e9562-2) unstable; urgency=medium

  * Revert to libssl 1.1 on upstream's request. This will be reverted as
    soon as the issues caused by openssl 1.1 are addressed. Until then,
    this Reopens: #870775.

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 28 Feb 2018 08:31:56 -0500

boxbackup (0.13~~git20180102.g6d7e9562-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * QA upload.
  * debian/boxbackup-server.config: Fix quoting of variables primarily
    fixing the logic that handles when tune2fs is not available.
  * Have boxbackup-server package Recommend e2fsprogs since the
    package configuration (optionally) uses tune2fs in some cases.
    (Closes: #887274)

  [ Reinhard Tartler ]
  * New upstream pre-release (requested by upstream)
  * Migrate to libssl 1.1 (Closes: #870775)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 20 Feb 2018 21:53:32 -0500

boxbackup (0.12~gitcf52058f-3) unstable; urgency=medium

  * QA upload.
  * Run the testsuite on amd64 and i386 only (Closes: #866372)

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 30 Jun 2017 15:01:19 -0400

boxbackup (0.12~gitcf52058f-2) unstable; urgency=medium

  * Fix FTBFS on signed arch architectures (Closes: #865092)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 20 Jun 2017 22:23:07 -0400

boxbackup (0.12~gitcf52058f-1) unstable; urgency=medium

  * QA upload.
  * Honor test failures during build (Closes: #744266)
  * New upstream release: 0.12 (Closes: #744911)
    - has openssl 1.1 support (Closes: #851092)
  * Update the notifyadmin.sh script (Closes: #483928)
  * Add systemd unit files.

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 17 Jun 2017 17:54:09 -0400

boxbackup (0.11.1~r2837-4) unstable; urgency=medium

  * QA upload.
  * Check for ucf and deluser availability before calling them during postrm
    purge.  (Closes: #688373, #667535)
  * Add Italian (it) debconf translations by Beatrice Torracca.
    (Closes: #658724)
  * Add Dutch (nl) debconf translations by Frans Spiesschaert.
    (Closes: #766532)
  * Build with dh_autotools-dev_*.
  * Switch to source format 3.0 (quilt).

 -- Andreas Beckmann <anbe@debian.org>  Sun, 22 Jan 2017 03:29:52 +0100

boxbackup (0.11.1~r2837-3) unstable; urgency=medium

  * QA upload.
  * Build with openssl 1.0. Thanks to Adrian Bunk. (Closes: #828253)
  * Bump Standards-Version to 3.9.8.

 -- Chris Lamb <lamby@debian.org>  Sat, 12 Nov 2016 14:15:06 +0000

boxbackup (0.11.1~r2837-2) unstable; urgency=medium

  * Ensure that LWP/UserAgent.pm is available (Closes: #816517)
  * Orphaning package, cf. #817078

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 07 Mar 2016 20:19:45 -0500

boxbackup (0.11.1~r2837-1) unstable; urgency=low

  * New upstream release.
    -  Bug fix: "Handling of "get" command broken due to bug in
       temp file name creation.", thanks to Matto Marjanovic
       (Closes: #606559).
  * Bug fix: "[INTL:pt_BR] Brazilian Portuguese debconf templates
    translation", thanks to Adriano Rafael Gomes (Closes: #610416).
  * Bug fix: "[INTL:da] Fix Danish translation of the debconf templates
    BoxBackup", thanks to Joe Dalton (Closes: #619303).
  * Bug fix: "Documentation errors regarding bbstored-certs", thanks to
    Clint Adams (Closes: #601882).
  * Build in verbose mode.
  * Bump standards version to 3.9.2, no changes needed.
  * Normalize fields in debian/control with wrap-and-sort(1).
  * implement build-arch and build-indep targets

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 28 Oct 2011 01:53:48 +0200

boxbackup (0.11~rc8~r2714-1) unstable; urgency=low

  * New upstream release.
  * New Upstream fixes Bug: "boxbackup-client complains about
    /home/*/.gvfs", thanks for reporting to Sune Mølgaard.
    Closes: #593401, LP: #496334
  * Enhancement: "Please use newer bdb", thanks to Clint Adams
    Closes: #548475
  * bin/bbstored/bbstored-certs: reduce root CA expiration date to avoid
    Y2k38 overflow. Thanks to Clint Adams <schizo@debian.org> for
    reporting it. Closes: #601506, LP: #641112
  * By default, do not manage boxbackup-client configuration via debconf
  * Allow dpkg-reconfigure to work even when the debconf level is set to
    'high'.
  * Various debconf severity fixes in boxbackup-*.conf

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 09 Nov 2010 18:14:58 +0100

boxbackup (0.11~rc3~r2502-4) unstable; urgency=low

  * use more globs in install paths to avoid failures on kFreeBSD
  * Bump standards version, no changes needed

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 13 Oct 2010 20:51:39 +0200

boxbackup (0.11~rc3~r2502-3) unstable; urgency=low

  * Update Vitnamese debconf translations. Closes: #598581

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 12 Oct 2010 10:50:37 +0200

boxbackup (0.11~rc3~r2502-2.1ubuntu1) maverick; urgency=low

  * fix install paths for armel (LP: #616461)

 -- David Sugar <david.sugar@canonical.com>  Mon, 16 Aug 2010 23:49:22 +0200

boxbackup (0.11~rc3~r2502-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * No longer hardcode path to tune2fs in config script
  * Fix pending l10n issues. Debconf translations:
    - Spanish (Omar Campagne).  Closes: #537589
    - Japanese (Hideki Yamane (Debian-JP)).  Closes: #558074
    - Danish (Joe Hansen).  Closes: #582174

 -- Christian Perrier <bubulle@debian.org>  Mon, 24 May 2010 09:33:23 +0200

boxbackup (0.11~rc3~r2502-2) unstable; urgency=low

  * Bug fix: "tries to contact a server build-time", thanks to
    Riku Voipio for reporting. Fix based on a patch contributed
    by peter green, thanks as well! (Closes: #525277).
  * Bug fix: "missing #include", thanks to Martin Michlmayr
    (Closes: #526152, LP: #371809)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 05 May 2009 07:42:40 +0200

boxbackup (0.11~rc3~r2502-1) unstable; urgency=low

  * New upstream release, built from svn revision 2502.
    - silently ignores sockets and named pipes (Closes: #479145)
    - syslog facility is now configurable (Closes: #521283)

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 02 Apr 2009 13:58:17 +0200

boxbackup (0.11~rc2+r2072-1) unstable; urgency=low

  * New upstream release. (Closes: #503942)
  * no longer use the upstream tarballs, but create them with the script
    debian/get-orig-source.sh to create the orig.tar.gz. See README.Debian
    for a rationale of this step.
  * Therefore, run autoconf on the buildds.
  * always use config.sub/config.guess from the autotools-dev package
  * Fake a "VERSION.txt" instead of letting the build system try to figure
    out the svn version by itself (which would fail)
  * no longer include patches to the upstream source inline. This doesn't
    work out too well with bzr. Port all patches to quilt instead.
  * factor out distribution independent cleaning rules to a new shell script
    called debian/clean.sh.
  * make debian/NEWS actually parsable by dpkg-parsechangelog. This should
    make apt-parsechangelog work.
  * Update debian specific documentation to new URL: http://boxbackup.org
  * Don't install the files DOCUMENTATION.txt and CONTACT.txt. Both contain
    just (outdated) contact addresses and URLs. (Closes: #503659)
  * add translation for swedish debconf messages. Translation provided by
    Martin Bagge <brother@bsnet.se>. (Closes: #513776)
  * adjust configure.ac to only AC_SUBST single variables instead of 3 in
    a row. Fixes FTBFS on sid, found at http://bugs.gentoo.org/205558

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 31 Mar 2009 15:58:23 +0200

boxbackup (0.11~rc2-6) unstable; urgency=low

  * Fix shell scripting in the debconf interaction code of the package's
    postinst script. This should prevent problems like LP: #222999

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 11 Feb 2009 08:55:05 +0100

boxbackup (0.11~rc2-5) unstable; urgency=low

  * Bugfix: "Please build-depend on docbook-xml". Thanks to Luca Falavigna
    <dktrkranz@ubuntu.com> for reporting. Closes: #507973
  * Add missing #includes in lib/common/Logging.cpp and
    lib/common/DebugMemLeakFinder.cpp. Also use malloc, free, etc from the
    namespace std:: instead of the global namespace in several other files
    Closes: #505696, #512510

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 22 Jan 2009 08:26:24 +0100

boxbackup (0.11~rc2-4) unstable; urgency=low

  * add a note to documentation about adjusted syslog facility.
  * build and install the administrator guide and the installation guide.
  * By default do not configure boxbackup. Most users of this package need
    to know what the postinst/configure scripts are doing anyway, and this
    avoids piuparts to fail here. Closes: #502742, #502461

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 23 Oct 2008 20:25:10 +0200

boxbackup (0.11~rc2-3.1) unstable; urgency=low

  * Non-maintainer upload to fix pending l10n issues.
  * Debconf translations:
    - German. Closes: #477719
    - French. Closes: #478557
    - Portuguese. Closes: #483869
    - Czech. Closes: #483981
    - Galician. Closes: #483857
    - Finnish. Closes: #484353
    - Basque. Closes: #484563
    - Russian. Closes: #484829

 -- Christian Perrier <bubulle@debian.org>  Tue, 20 May 2008 07:54:18 +0200

boxbackup (0.11~rc2-3) unstable; urgency=low

  * fix another miss '#include <string.h>' to fix compilation with g++-4.3.

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 23 Apr 2008 13:10:17 +0200

boxbackup (0.11~rc2-2) unstable; urgency=low

  * merge forgotten changes from the unstable Branch. (sorry to the
    translators for the confusion).
  * The previous upload forgot to change some references to the default
    configuration directory from /etc/box -> /etc/boxbackup. Now rectified.
  * add an '#include <string.h>' in ./lib/common/Logging.cpp. Should fix
    build failiures on mips, powerpc and sparc.

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 21 Apr 2008 13:41:18 +0200

boxbackup (0.11~rc2-1) unstable; urgency=low

  * new upstream release.
  * update watch file.
  * bump standards version to 3.7.3 (No changes needed)
  * update frensh debconf template translations. Thanks to
    Christian Perrier (Closes: #476090)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 20 Apr 2008 14:01:27 +0200

boxbackup (0.11~rc1-1) experimental; urgency=low

  * New upstream release.
    - should fix builds on kFreeBSD architectures: (Closes: #440156).
    - build against libbd4.6 instead of libdb4.3. (Closes: #442640).
    - the config file template has been updated to be more specific for
      the AlwaysIncludeFile Option (Closes: #435860).
  * remove all generated files in clean target of debian/rules. Allows
    package to build twice in a row (Closes: #442515).
  * Install file ExceptionCodes.txt to documentation directories. The file
    contains a list of Error codes found in logfiles.
  * Simplify debian/rules by removing code to build arch-independent
    packages. There are none.

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 20 Jan 2008 19:09:59 +0100

boxbackup (0.10+really0.10-2) unstable; urgency=medium

  * raise urgency because of RC bug!
  * use rather 'nocheck' instead of 'notest'. Thanks to Michael Banck
    <mbanck@debian.org> for notifying me about this.
  * Ack NMUs. Thanks Amaya for handling these! (Closes: #470060, #467628).
  * Merge Ubuntu changes, fixing installations without tty. (Closes: #474587).
  * build against libdb4.6 (Closes: #442640)
  * don't ignores errors on clean. Thanks lintian.
  * update Standards Version to 3.7.3, no changes needed.
  * clarify debconf question "boxbackup-client/IncorrectNumber" (Closes: #467635).
  * clarify debconf question "boxbackup-server/raidBlockSize" (Closes: #467636).

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 13 Apr 2008 11:38:01 +0200

boxbackup (0.10+really0.10-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS introduced by my previous NMU. Apply patch from Cyril Brulebois
    (Closes: #454862).
  * Added debian debconf translation by Kai Wasserbäch (Closes: #467628).

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Sat, 05 Apr 2008 12:37:16 +0200

boxbackup (0.10+really0.10-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * iFix LSB header in init.d script (Closes: #470060).

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Mon, 31 Mar 2008 18:43:40 +0200

boxbackup (0.10+really0.10-1ubuntu3) hardy; urgency=low

  * Don't redirect ucf calls.  This was forgotten in the previous upload,
    but should have been there.

 -- Tollef Fog Heen <tfheen@ubuntu.com>  Thu, 06 Mar 2008 07:17:35 +0100

boxbackup (0.10+really0.10-1ubuntu2) hardy; urgency=low

  * Fix up postinst so we don't call db_stop too early, and use
    --debconf-ok to ucf.  (No versioned dependency since even oldstable
    has a new enough ucf.)  Use db_stop near the end to make sure we don't
    hang after starting the daemon though.

 -- Tollef Fog Heen <tfheen@ubuntu.com>  Wed, 05 Mar 2008 22:33:57 +0100

boxbackup (0.10+really0.10-1ubuntu1) hardy; urgency=low

  * Rebuild for libdb4.3 -> libdb4.6 migration.
  * Set MOTU to maintainer.

 -- Chuck Short <zulcss@ubuntu.com>  Mon, 03 Mar 2008 12:37:35 -0500

boxbackup (0.10+really0.10-1) unstable; urgency=low

  * revert new upstream accidentally slipped into unstable.
  * big apologies that I now need to upload this package earlier than
    promised to the boxbackup translators :(
  * add watchfile
  * Bug fix: "boxbackup-server recommends boxbackup-utils (unavailable)",
    thanks to Pelayo Gonzalez (Closes: #424992).
  * Apply new debconf templates and debian/control review. Thanks to
    Christan Perrier! (Closes: #429396)
  * Bug fix: "boxbackup: French debconf templates translation", thanks to
    Vincent Bernat (Closes: #430856).
  * Bug fix: "[l10n] Czech translation of boxbackup debconf messages",
    thanks to Miroslav Kure (Closes: #431463).
  * Bug fix: "boxbackup: [INTL:vi] Vietnamese debconf templates
    translation", thanks to Clytie Siddall (Closes: #430535).
  * Bug fix: "depends on non-essential package ucf in postrm", thanks to
    Michael Ablassmeier (Closes: #431518).
  * Bug fix: "depends on non-essential package ucf in postrm", thanks to
    Michael Ablassmeier (Closes: #431519).
  * run testsuite on build, use 'notest' in $DEB_BUILD_OPTIONS to disable

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 03 Jul 2007 12:30:49 +0200

boxbackup (0.10-1) unstable; urgency=low

  * upload to debian (Closes: #416605)
  * Cleanups in debian/rules
  * Apply patch from svn, commit #626, in order to fix FTBFS
    see http://bbdev.fluffy.co.uk/trac/changeset/626
  * Drop the boxbackup-utils package, since it only shipped one single
    script, which is generally used for CA maintenance, so move it to the
    to the boxbackup-server package
  * Bump standards version to 3.7.2
  * add missing manpages
  * add README.Debian
  * use debhelper 5
  * cleanup the versioned dependencies in debian/control

  [ Jérôme Schell ]

  * New upstream version
  * Add LSB headers in init script

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 18 Jun 2007 15:35:55 +0100

boxbackup (0.09-3) unstable; urgency=low

  * Added man pages for bbackupd, bbackupd-config, bbackupctl, bbackupquery
  * Improve lintian compatibility of the packages

 -- Jérôme Schell <jerome@myreseau.org>  Mon, 10 Oct 2005 14:16:20 +0200
